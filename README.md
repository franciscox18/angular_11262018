# Welcome to Advanced Angular Class!

## Instructor

Eric Greene

## Schedule

Class:

- Wednesday - Friday: 9:15am to 5pm

Breaks:

- Morning Break: 10:30am to 10:40am
- Lunch: 11:45am to 12:30pm
- Afternoon Break #1: 2:00pm to 2:10pm
- Afternoon Break #2: 3:00pm to 3:10pm

## Course Outline

- Day 1 - Overview of Angular, Angular CLI, Modules, Components, Composition
- Day 2 - Composition (cont'd)
- Day 3 - Composition Review, Pipes, Reactive Forms, Services
- Day 4 - Asynchronous Programming, REST Service Calls, RxJS
- Day 5 - Routing, Unit Testing, Other Topics

## Links

### Instructor's Resources

- [Accelebrate, Inc.](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016
- [Microsoft Virtual Academy](https://mva.microsoft.com/search/SearchResults.aspx#!q=Eric%20Greene&lang=1033)

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)

## Useful Resources

- [Angular CLI](https://cli.angular.io/)
- [TypeScript Coding Guidelines](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines)
- [Angular Style Guide](https://angular.io/docs/ts/latest/guide/style-guide.html)
- [Angular Cheat Sheet](https://angular.io/docs/ts/latest/guide/cheatsheet.html)
- [Angular API](https://angular.io/docs/ts/latest/api/)
