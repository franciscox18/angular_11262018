import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myUppercase',
  pure: false,
})
export class MyUppercasePipe implements PipeTransform {

  transform(value: any): any {

    // console.log('my-uppercase pipe was called:', value);

    if (value == null || String(value).length === 0) {
      return '';
    }

    return String(value).toUpperCase();
  }

}
