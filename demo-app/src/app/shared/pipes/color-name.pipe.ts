import { Pipe, PipeTransform } from '@angular/core';

import colorNamer from 'color-namer';

@Pipe({
  name: 'colorName'
})
export class ColorNamePipe implements PipeTransform {

  transform(value: any): any {
    return colorNamer(value).html[0].name;
  }

}
