import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myAppend'
})
export class MyAppendPipe implements PipeTransform {

  transform(value: any, valueToAppend): any {
    return String(value) + String(valueToAppend);
  }

}
