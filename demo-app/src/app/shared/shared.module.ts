import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolHeaderComponent } from './components/tool-header/tool-header.component';
import { MyUppercasePipe } from './pipes/my-uppercase.pipe';
import { MyAppendPipe } from './pipes/my-append.pipe';
import { ColorNamePipe } from './pipes/color-name.pipe';

@NgModule({
  declarations: [
    ToolHeaderComponent,
    MyUppercasePipe,
    MyAppendPipe,
    ColorNamePipe,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ToolHeaderComponent,
    MyUppercasePipe,
    ColorNamePipe,
  ],
})
export class SharedModule { }
