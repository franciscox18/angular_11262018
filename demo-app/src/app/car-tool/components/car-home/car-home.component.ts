import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, switchMap, mapTo, map } from 'rxjs/operators';

import { Car } from '../../models/car';
import { CarsService } from '../../services/cars.service';

@Component({
  selector: 'car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent implements OnInit {

  refreshCars = new BehaviorSubject<Car[]>([]);

  cars$: Observable<Car[]> = this.refreshCars.pipe(
    switchMap(() => this.carsSvc.all()),
  );

  get editCarId() {
   return  this.carsSvc.editCarId;
  }

  constructor(private carsSvc: CarsService) { }

  // refreshCars() {
  //   return this.carsSvc.all().subscribe(cars => {
  //     return this.cars = cars;
  //   });
  // }

  ngOnInit() {
    this.refreshCars.next([]);
  }

  doAddCar(car: Car) {
    this.carsSvc.append(car).pipe(
      // tap(() => this.carsSvc.edit(-1)),
      // mapTo([]),
      map(() => {
        this.carsSvc.edit(-1);
        return [];
      })
    ).subscribe(this.refreshCars);
  }

  doEditCar(carId: number) {
    this.carsSvc.edit(carId);
  }

  doCancelCar() {
    this.carsSvc.edit(-1);
  }

  doReplaceCar(car: Car) {
    // this.carsSvc.replace(car).pipe(
    //   tap(() => this.carsSvc.edit(-1)),
    //   switchMap(() => this.carsSvc.all())
    // ).subscribe((cars: Car[]) => {
    //   this.cars = cars;
    // });
  }

  doDeleteCar(carId: number) {
    // this.carsSvc.delete(carId).pipe(
    //   tap(() => this.carsSvc.edit(-1)),
    //   switchMap(() => this.carsSvc.all())
    // ).subscribe((cars: Car[]) => {
    //   this.cars = cars;
    // });

  }
}
