import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';

import { Color } from '../../models/color';

const myRequired = (c: AbstractControl) => {

  if (c.value == null || String(c.value).length === 0) {
    return {
      myRequired: true,
    };
  }

  return null;

};


@Component({
  selector: 'color-form',
  templateUrl: './color-form.component.html',
  styleUrls: ['./color-form.component.css']
})
export class ColorFormComponent implements OnInit {

  @Input()
  buttonText = 'Submit Color Form';

  @Output()
  submitColor = new EventEmitter<Color>();

  colorForm: FormGroup;

  constructor(private fb: FormBuilder) {

    // const colorName = new FormControl('', Validators.required);
    // const colorHexCode = new FormControl('');

    // this.colorForm = new FormGroup({
    //   colorName,
    //   colorHexCode,
    // });

    this.colorForm = this.fb.group({
      colorName: [ '', { validators: [ myRequired, Validators.minLength(3) ] } ],
      colorHexCode: '',
    });

  }

  ngOnInit() {
  }

  doSubmitColor() {
    this.submitColor.emit({
      name: this.colorForm.value.colorName,
      hexCode: this.colorForm.value.colorHexCode,
    });

    this.colorForm.reset();
  }

}
