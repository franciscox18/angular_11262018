import { Component, OnInit } from '@angular/core';

import { Color } from '../../models/color';
import { ColorsService } from '../../services/colors.service';

@Component({
  selector: 'color-home',
  templateUrl: './color-home.component.html',
  styleUrls: ['./color-home.component.css']
})
export class ColorHomeComponent implements OnInit {

  otherHeaderText = 'Eric\'s Color Tool';

  colors: Color[] = [];

  constructor(private colorsSvc: ColorsService) {}

  ngOnInit() {
    this.colors = this.colorsSvc.all();
  }

  addColor(color: Color) {
    this.colors = this.colors.concat({
      ...color,
      id: Math.max(...this.colors.map(c => c.id), 0) + 1,
    });
  }

  deleteColor(colorId: number) {
    this.colors = this.colors.filter(c => c.id !== colorId);
  }

}
