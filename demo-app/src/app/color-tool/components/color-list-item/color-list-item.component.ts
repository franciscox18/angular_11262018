import { Component, OnInit, Input, Output, EventEmitter, DoCheck } from '@angular/core';

import { Color } from '../../models/color';

@Component({
  selector: '.color-list-item',
  templateUrl: './color-list-item.component.html',
  styleUrls: ['./color-list-item.component.css']
})
export class ColorListItemComponent implements OnInit, DoCheck {

  @Input()
  color: Color;

  @Output()
  deleteColor = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  ngDoCheck() {
    console.log('change detection color list item');
  }

  doDeleteColor() {
    this.deleteColor.emit(this.color.id);
  }

}
