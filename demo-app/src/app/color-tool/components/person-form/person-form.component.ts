import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { validatorSpecifyEmailOrPhone } from '../../../shared/validators/emailOrPhone';

@Component({
  selector: 'person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css']
})
export class PersonFormComponent implements OnInit {

  personForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.personForm = this.fb.group({
      email: '',
      phone: '',
    }, {
      validator: [ validatorSpecifyEmailOrPhone ],
    });
  }

  get hasErrors() {
    return this.personForm.errors !== null;
  }

  get errorMessages() {
    return Object.keys(this.personForm.errors).map(key => this.personForm.errors[key]);
  }

}
