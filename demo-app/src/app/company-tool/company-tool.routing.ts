import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from './components/home-page/home-page.component';
import { AboutPageComponent } from './components/about-page/about-page.component';
import { ContactPageComponent } from './components/contact-page/contact-page.component';

const routes: Routes = [
  { path: 'home', component: HomePageComponent, children: [
    { path: 'contact', component: ContactPageComponent }
  ] },
  { path: 'about/:id', component: AboutPageComponent },
  { path: 'contact', component: ContactPageComponent },
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  // { path: '**', component: PageNotFoundComponent },
];

export const CompanyToolRoutingModule = RouterModule.forChild(routes);

