Exercise 1

https://gitlab.com/t4d-classes/angular_11262018

1. Create a new module named car-tool.

> ng g m color-tool

2. Create a new component named car-home in car-tool.

> ng g component color-tool/components/color-home

3. Export car-home from car-tool and display the car-home component beneath the color-home component in app component.

4. Add a property named 'headerText' to car-home and populate with 'Car Tool'. Display the header in car-home component.

5. Ensure it all works.