Exercise #8

1. Change the make field of the car form to be a drop down of car makes. The HTML element you should use is the select element.

2. Change the model field to be a required field with a min length of 5 characters.

3. Change the color field to be an input type of color.

4. Change the year field to have a min value of the year cars were first invented.

5. The price of a car should not be less than 0 and not more than $100000 dollars.

6. For validation errors please use either CSS or ngIf to display the messages with at least one example of each.

Extra Credit:

7. For the car table, create a pipe which translates the HTML hexcode generated by the color input field to a color name.

npm install color-namer --save

import colorNamer from 'color-namer';